package validation;

public interface Validate<T> {
    boolean isValid(T entity);
}
