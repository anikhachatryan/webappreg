package entity;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private final String name;
    private final String lastname;
    private final String email;
    private final String password;

    private User(final UserBuilder builder) {
        this.name = builder.name;
        this.lastname = builder.lastname;
        this.email = builder.email;
        this.password = builder.password;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return email.equals(user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public static class UserBuilder {
        private String name;
        private String lastname;
        private String email;
        private String password;
        public UserBuilder() {
        }

        public UserBuilder setName(String name){
            this.name = name;
            return this;
        }

        public UserBuilder setLastname(String lastname){
            this.lastname = lastname;
            return this;
        }

        public UserBuilder setEmail(String email){
            this.email = email;
            return this;
        }

        public UserBuilder setPassword(String password){
            this.password = password;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
