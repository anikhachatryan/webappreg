package servlet;

import entity.User;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        String name = req.getParameter("name");
        String lastname = req.getParameter("lastname");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        UserService userService = UserService.getInstance();
        User user = new User.UserBuilder().setName(name).setLastname(lastname).setEmail(email).setPassword(password).build();
        if (userService.isValid(user)) {
            HttpSession session = req.getSession();
            session.setAttribute("email", email);
            session.setAttribute("password", password);
            session.setAttribute("name", name);
            session.setAttribute("lastname", lastname);
            session.setAttribute("from", req.getRequestURI());
            RequestDispatcher rs = req.getRequestDispatcher("/signIn");

            rs.forward(req, resp);
        } else resp.sendRedirect("view/html/ErrorPage.html");

    }
}
