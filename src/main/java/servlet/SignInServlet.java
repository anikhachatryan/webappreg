package servlet;

import entity.User;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        Object from = req.getSession().getAttribute("from");
        System.out.println(from);
        if (from!=null && from.equals("/webAppReg/registration")) {
            HttpSession session = req.getSession(false);

            String name = (String) session.getAttribute("name");
            String lastname = (String) session.getAttribute("lastname");
            String email = (String) session.getAttribute("email");
            String password = (String) session.getAttribute("password");

            UserService userService = UserService.getInstance();
            User user = new User.UserBuilder().setName(name).setLastname(lastname).setEmail(email).setPassword(password).build();
            if (!userService.contains(email)) {
                userService.add(user);
                resp.sendRedirect("view/html/LoggedIn.html");
            } else if (userService.contains(email)) {
                session.setAttribute("from", req.getRequestURI());
                resp.sendRedirect("view/html/HaveAccount.html");
            } else {
                resp.sendRedirect("view/html/ErrorPage.html");
            }
        }else {

            String email = req.getParameter("email");
            String password = req.getParameter("password");
            UserService userService = UserService.getInstance();
            if (userService.contains(email) && userService.check(email, password)) {
                resp.sendRedirect("view/html/LoggedIn.html");
            }else resp.sendRedirect("view/html/ErrorPage.html");

        }
    }
}
