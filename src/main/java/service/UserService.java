package service;

import entity.User;
import validation.Validate;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public final class UserService implements Validate<User> {

    private List<User> list = new ArrayList<>();
    private final String FILEPATH = "resources/storage.txt";

    private static UserService instance;

    private UserService() {
        Path path = Paths.get(FILEPATH);
        File fileChecker = new File(FILEPATH);

        if (fileChecker.exists()) {

            System.out.println(fileChecker.getAbsolutePath());
            if (fileChecker.length() != 0) {
                ObjectInputStream inputStream = null;
                try {
                    inputStream = new ObjectInputStream(new FileInputStream(FILEPATH));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                while (true) {
                    try {
                        list = (List<User>) inputStream.readObject();
                        System.out.println(list);
                    } catch (IOException ex) {
                        break;
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace();
                    }

                }
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                Files.createDirectories(path.getParent());
                Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public boolean add(User user) {
        if (this.isValid(user)) {
            list.add(user);
            saveList();
            return true;
        }
        return false;
    }

    public boolean delete(String email) {
        for (User user : list) {
            if (user.getEmail().equals(email)) {
                list.remove(user);
                return true;
            }
        }

        return false;
    }

    public boolean contains(String email) {
        for (User user : list) {
            if (user.getEmail().equals(email)) {
                return true;
            }
        }
        return false;
    }

    public boolean check(String email, String password) {
        for (User user : list) {
            if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    private void saveList() {

        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(FILEPATH));
            outputStream.writeObject(list);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<User> getList() {
        return list;
    }

    public boolean isValid(User user) {
        return user.getName() != null && user.getLastname() != null && user.getEmail() != null && user.getPassword() != null;
    }
}
