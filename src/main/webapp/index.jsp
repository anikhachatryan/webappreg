<html>
<head>
    <title>Welcome</title>
    <link href="view/css/IndexMain.css" rel="stylesheet">
</head>
<body>
<form action="registration" method="post">
    <h3>Registration Form</h3>
    <div class="parentDiv">
        <input type="text" name="name" placeholder="First name" required>
        <input type="text" name="lastname" placeholder="Last name" required>
        <input type="email" name="email" placeholder="Email" required>
        <input type="password" name="password" placeholder="Password" required>
        <input type="submit" value="Register" class="click">

    </div>
</form>
<div class="haveAccount">
    <label for="sign">Already have an account? </label>
    <a href="view/jsp/signInForm.jsp" id="sign">Sign in</a>
</div>
</body>
</html>
